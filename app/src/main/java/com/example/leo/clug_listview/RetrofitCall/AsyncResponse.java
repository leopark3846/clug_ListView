package com.example.leo.clug_listview.RetrofitCall;


import com.example.leo.clug_listview.googlemapsretrofit.Example;

import retrofit2.Response;

/**
 * Created by leo on 2017-07-17.
 */

public interface AsyncResponse {
    // 결과물을 activity에서 받아오기 위한 interface
    void processFinish(Response<Example> response);
}
