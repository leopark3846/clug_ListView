package com.example.leo.clug_listview.RetrofitCall;

import com.example.leo.clug_listview.googlemapsretrofit.Example;

import java.util.Map;

import retrofit2.Call;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;
import retrofit2.http.GET;
import retrofit2.http.QueryMap;

/**
 * Created by leo on 2017-07-16.
 */

public interface GooglePlaceService {
    // retrofit 객체 빌드하기 위한 interface
    // get 요청 보낼 나머지 주소
    @GET("maps/api/place/textsearch/json")
    Call<Example> getPlaces(
            // query 값 옵션, QueryMap을 이용하여 HashMap 값을 받아옴
            @QueryMap Map<String, String> options
    );
    public static final Retrofit retrofit = new Retrofit.Builder()
            // 요청 보낼 base 주소
            .baseUrl("https://maps.googleapis.com/")
            .addConverterFactory(GsonConverterFactory.create())
            .build();

}
