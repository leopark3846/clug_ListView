package com.example.leo.clug_listview;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;

import com.example.leo.clug_listview.RetrofitCall.AsyncResponse;
import com.example.leo.clug_listview.RetrofitCall.GooglePlaceService;
import com.example.leo.clug_listview.RetrofitCall.NetworkCall;
import com.example.leo.clug_listview.googlemapsretrofit.Example;
import com.example.leo.clug_listview.googlemapsretrofit.Result;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

import retrofit2.Call;
import retrofit2.Response;

public class ListViewActivity extends AppCompatActivity implements AsyncResponse {
    // 검색결과 json의 Result를 저장하기 위한 ArrayList
    ArrayList<Result> resultList = new ArrayList<Result>();
    // listview1에 Result에서 title만 출력하기 위한 ArrayList
    ArrayList<String> LIST_MENU = new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_view);
        // listview1, button2, editText 객체 설정
        ListView listview = (ListView) findViewById(R.id.listview1) ;
        Button addBtn = (Button) findViewById(R.id.button2);
        final EditText searchText = (EditText) findViewById(R.id.editText);

        // button2 눌렀을 때 Listener
        addBtn.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                EditText searchPlaceText = (EditText) findViewById(R.id.editText);
                // 검색할 때 쓸 parameter HashMap 통해서 넣어주기
                Map<String, String> d = new HashMap<String, String>();
                d.put("query", searchText.getText().toString());
                d.put("key", "AIzaSyBx7DqG04qR-hXB6wZpyW7pJhDB3OTelC4");

                // retrofit 빌드하기
                GooglePlaceService googlePlaceService = GooglePlaceService.retrofit.create(GooglePlaceService.class);
                // 만들어놓은 HashMap 전달해서 Call 객체 생성
                final Call<Example> call = googlePlaceService.getPlaces(d);
                // background단에서 통신 수행 위한 thread 생성
                NetworkCall n = new NetworkCall();
                // response 받은 객체 받아오기 위한 delegate를 ListViewActivity로 설정
                n.delegate = ListViewActivity.this;
                // 서비스 통신 수행
                n.execute(call);

            }

        });


    }
    public void processFinish(Response<Example> response){
        try{
            // 정보 갱신하기 위해서 listview1 객체 설정
            ListView listview = (ListView) findViewById(R.id.listview1) ;
            // listview1 어댑터 설정
            ArrayAdapter adapter = new ArrayAdapter(this, android.R.layout.simple_list_item_1, LIST_MENU) ;
            listview.setAdapter(adapter) ;
            // 검색결과 중 최상단 장소를 resultList에 추가
            resultList.add(response.body().getResults().get(0));
            // 최상단 장소의 이름을 LIST_MENU에 추가
            LIST_MENU.add(resultList.get(resultList.size()-1).getName());

            Log.d("resultList", "size : " + String.valueOf(resultList.size()));
            Log.d("resultList", "last element : " + String.valueOf(LIST_MENU.get(LIST_MENU.size()-1)));
            //listview1 adapter 새로 갱신
            adapter.notifyDataSetChanged();
        } catch (Exception e){
            // 정상적으로 통신 수행이 되지 않았을 때
            Log.e("My App", "Could not parse malformed JSON: \"" + response.body().toString() + "\"");
        }
    }
}
