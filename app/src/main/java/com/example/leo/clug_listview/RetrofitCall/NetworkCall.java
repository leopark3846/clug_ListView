package com.example.leo.clug_listview.RetrofitCall;

import android.os.AsyncTask;
import android.util.Log;

import com.example.leo.clug_listview.googlemapsretrofit.Example;


import retrofit2.Call;
import retrofit2.Response;

/**
 * Created by leo on 2017-07-17.
 */

public class NetworkCall extends AsyncTask<Call, Void, Response<Example>> {
    public AsyncResponse delegate = null;
    @Override
    protected Response<Example> doInBackground(Call... params){
        try {
            // 스레드 실행
            // 인수로 받은 call을 받아옴
            Call<Example> call = params[0];
            // call 실행
            Response<Example> response = call.execute();
            return response;
        } catch (Exception e){
            Log.d("AsyncTask", e.getMessage());
        }
        return null;
    }
    @Override
    protected void onPostExecute(Response<Example> response){
        // 스레드 종료 시에 실행됨
        // ListViewActivity에서 response(검색결과물)을 매개변수로 받아서 실행
        // 원래 activity에서는 background단에서 결과물을 바로 받을 수 없음
        delegate.processFinish(response);
    }
}
